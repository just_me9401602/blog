from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.generics import RetrieveUpdateAPIView
from .models import Post, Comment
from .serializers import PostSerializer, CommentSerializer, UserSerializer
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAdminUser, IsAuthenticated
from .permissions import isOwnerOrReadOnly
from django.contrib.auth.models import User

# Create your views here.
class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['author']
    permission_classes = [IsAuthenticatedOrReadOnly & isOwnerOrReadOnly | IsAdminUser]

class CommentViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['post', 'author']
    permission_classes = [IsAuthenticatedOrReadOnly & isOwnerOrReadOnly | IsAdminUser]

class UserView(RetrieveUpdateAPIView):
    model = User
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]
    
    def get_object(self):
        obj = self.request.user
        return obj
