from django.db import models
from django.contrib.auth.models import User

class Post(models.Model):
    title = models.CharField(max_length=100, null=False, blank=False, verbose_name='Title')
    content = models.TextField(null=True, blank=True, verbose_name='Content')
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='posts', verbose_name='Author')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Creation date and time')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Update date and time')

    def __str__(self) -> str:
        return self.title
    
class Comment(models.Model):
    text = models.TextField(max_length=1000, null=False, blank=False, verbose_name='New Comment')
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='comments', verbose_name='Author')
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comments', verbose_name='Post')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Creation date and time')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Update date and time')

    def __str__(self) -> str:
        return self.text[:20]
