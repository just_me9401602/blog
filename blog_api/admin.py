from django.contrib import admin

from blog_api.models import Post, Comment

admin.site.register(Post)
admin.site.register(Comment)
# Register your models here.
